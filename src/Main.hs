module Main where

import Data.List
import GHC.IO.Encoding (setLocaleEncoding, utf8)
import Hakyll
import Text.Pandoc.Highlighting
import Text.Pandoc.Options

customPandocCompiler = pandocCompilerWith readerOptions writerOptions
  where
    extensions =
        githubMarkdownExtensions <>
            (extensionsFromList
                [ Ext_inline_code_attributes
                , Ext_latex_macros
                , Ext_tex_math_double_backslash ])
    readerOptions = defaultHakyllReaderOptions
        { readerExtensions = extensions }
    writerOptions = defaultHakyllWriterOptions
        { writerExtensions = extensions
        , writerHTMLMathMethod = MathJax "" }

-- | The context applied to blog posts
postContext :: Context String
postContext =
    mconcat
        [ dateField "published" "%B %e, %Y"
        , defaultContext ]

-- | A helper function for applying the top-level template (the one containing
-- the `html` element)
applyMainTemplate :: Context a -> Item a -> Compiler (Item String)
applyMainTemplate = loadAndApplyTemplate $ fromFilePath "templates/main.html"

-- | The rules applied to pages of importance, such as index.html
specialPageRules :: String -> Rules ()
specialPageRules filename =
    match (fromGlob filename) $ do
        route $ setExtension "html"
        compile $ getResourceBody
            >>= applyMainTemplate defaultContext
            >>= relativizeUrls

-- | Entry point of the program
main :: IO ()
main = do
    setLocaleEncoding utf8
    hakyll $ do
        create [fromFilePath "css/highlighting.css"] $ do
            route idRoute
            compile $ makeItem (styleToCss tango)

        specialPageRules "index.html"

        create [fromFilePath "posts.html"] $ do
            route idRoute
            compile $ makeItem ()
                >>= loadAndApplyTemplate
                        (fromFilePath "templates/posts.html")
                         (listField "posts" postContext
                         $ loadAll (fromGlob "posts/*.md") >>= recentFirst)
                >>= (applyMainTemplate
                     $ mconcat
                         [ constField "title" "Posts"
                         , defaultContext ])
                >>= relativizeUrls

        match (fromGlob "css/*") $ do
            route idRoute
            compile compressCssCompiler

        match (fromGlob "images/*") $ do
            route idRoute
            compile copyFileCompiler

        match (fromGlob "posts/*.md") $ do
            route $ setExtension "html"
            compile $ customPandocCompiler
                >>= loadAndApplyTemplate
                        (fromFilePath "templates/post.html")
                        postContext
                >>= applyMainTemplate defaultContext
                >>= relativizeUrls

        match (fromGlob "templates/*") $ do
            compile templateBodyCompiler
