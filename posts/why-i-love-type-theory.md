---
title: Why I love type theory
published: February 08, 2019
summary:
  Discover the connection between type theory and algebra, logic, and category
  theory.
---

I'm interested in a field of math and computer science called type theory. I
would like to share why I find type theory beautiful and fascinating.

## Type theory as an alternative to set theory

Type theory was thought up by Bertrand Russell as an alternative to Georg
Cantor's naive set theory. Russell had discovered a paradox, known as Russell's
Paradox, that made set theory inconsistent. Russell's Paradox describes a set of
all sets that don't contain themselves; does this set contain itself? With type
theory, Russell created a hierarchy of "universes" to prevent this paradox; a
type cannot have itself as its own type.

Note that Zermelo-Fraenkel Set Theory does not have Russell's Paradox.

Bertrand Russell wasn't the only person who discovered this paradox; Cantor and
Zermelo did so as well.

Further reading: [Types as
Sets](https://guide.elm-lang.org/appendix/types_as_sets.html) in the Elm
programming language's documentation

## Types and terms

Alonzo Church invented the lambda calculus as a logic. His first iteration, the
untyped lambda calculus, is the one that most often comes to mind when we think
of the lambda calculus. The untyped lambda calculus consists of:

\\[
\begin{align}
term ::= \: & \lambda x . term && \textbf{Lambda abstraction} \\
            & term~term        && \textbf{Function application} \\
            & x, y, z          && \textbf{Variable} \\
\end{align}
\\]

The lambda calculus is a *term rewriting system* with *reduction rules*. A
lambda calculus program that cannot be reduced further is called a *normal
form*. The evaluation of a lambda calculus program is called *normalization*.

However, the not all programs in the untyped lambda calculus have a normal form.
For some programs, one can keep rewriting them forever and never reach a normal
form. Such programs are *nonterminating*. The untyped lambda calculus is
inconsistent as a logic due to nontermination. Consequently, Alonzo Church
devised the simply typed lambda calculus:

\\[
\begin{align}
\tau ::= \: & \tau \to \tau           && \textbf{Function type} \\
            & A, B, C                 && \textbf{Base type} \\
\\
term ::= \: & \lambda x : \tau . term && \textbf{Lambda abstraction} \\
            & term~term               && \textbf{Function application} \\
            & x, y, z                 && \textbf{Variable} \\
\end{align}
\\]

Types classify terms; terms are said to have a type. The rules of the type
system restrict what programs may be expressed as a way of ensuring program
correctness.

The type system of the simply typed lambda calculus rejects nonterminating
programs, making it sound as a logic. The simply typed lambda calculus is
*strongly normalizing*, meaning that every program has a normal form.

There are many other lambda calculi with different type systems. Examples are
System F, System F\\(_{\omega}\\), and the Calculus of Constructions.
These lambda calculi have varying expressive power due to the ability to write
functions from types to terms (the parametric polymorphism of System F), types
to types (the higher-kinded types of System F\\(_{\omega}\\)), or even terms to
types (dependent types). These lambda calculi are classified according to the
"lambda cube."

## The algebra of types

The terms of a certain type are called the type's *inhabitants*. One can do
algebra on types and count their inhabitants.

- The empty or bottom type has zero inhabitants, making it the \\(0\\) type.
- The unit type has one inhabitant, written \\(()\\), making it the \\(1\\)
  type.
- The disjoint union of types \\(A\\) and \\(B\\) has an inhabitant for each
  inhabitant of \\(A\\) and each inhabitant of \\(B\\). If \\(A\\) has \\(a\\)
  inhabitants and \\(B\\) has \\(b\\) inhabitants, their disjoint union has
  \\(a + b\\) inhabitants, making it the sum type, written \\(A + B\\).
- The pair type of types \\(A\\) and \\(B\\) has an inhabitant for each
  possible combination of \\(A\\)  and \\(B\\). If \\(A\\) has \\(a\\)
  inhabitants and \\(B\\) has \\(b\\) inhabitants, their pair type has
  \\(a \times b\\) inhabitants, making it the Cartesian product type, written
  \\(A \times B\\).
- The function type \\(A \to B\\) has \\(b^a\\) inhabitants where \\(A\\) has
  \\(a\\) inhabitants and \\(B\\) has \\(b\\) inhabitants, making it the
  exponential type.

Let's do an example. \\(Bool\\) has \\(2\\) inhabitants, \\(True\\) and
\\(False\\). \\(Bool \times Bool\\) has \\(4\\), or \\(2 \times 2\\)
inhabitants:

- \\((True, True)\\)
- \\((True, False)\\)
- \\((False, True)\\)
- \\((False, False)\\)

Let the sum \\(A + B\\) either be \\(Left~A\\) or \\(Right~B\\).

The sum of two \\(Bool\\)s also has \\(4\\) inhabitants, because
\\(2 + 2 = 4\\).

- \\(Left~True\\)
- \\(Left~False\\)
- \\(Right~True\\)
- \\(Right~False\\)

We then say that \\(Bool \times Bool\\) and \\(Bool + Bool\\) are *isomorphic*.
We can write inverse functions between them:

\\[
\begin{align}
& f : Bool \times Bool \to Bool + Bool\\
& f (True, True) = Left~True\\
& f (True, False) = Left~False\\
& f (False, True) = Right~True\\
& f (False, False) = Right~False\\
\\
& f^{-1} : Bool + Bool \to Bool \times Bool\\
& f^{-1} (Left~True) = (True, True)\\
& f^{-1} (Left~False) = (True, False)\\
& f^{-1} (Right~True) = (False, True)\\
& f^{-1} (Right~False) = (False, False)
\end{align}
\\]

Further reading: [The algebra (and calculus!) of algebraic data
types](https://codewords.recurse.com/issues/three/algebra-and-calculus-of-algebraic-data-types)
by Joel Burget

### The algebra of dependent types

The connection goes further. With dependent types, we can mix types and terms.
Thus, we introduce the dependent product and dependent sum types.

Confusingly, the dependent product type is a generalization of the exponential,
or function type, and the dependent sum type is a generalization of the product
type. However, the reason why will soon become crystal clear.

The dependent sum type is written as:

- \\((x : A) \times B(x)\\)
- \\(\sum \nolimits_{x : A} B(x)\\)

The dependent product type is written as:

- \\((x : A) \to B(x)\\)
- \\(\prod \nolimits_{x : A} B(x)\\)

\\(B\\) is actually a function of \\(A\\); it is said to *depend* on \\(A\\).

An exponential or function type is really a dependent product type where \\(B\\)
is a constant function independent of \\(A\\). A (non-dependent) product or
pair type is really a dependent sum type where \\(B\\) is a constant function as
well.

When we use numbers, sigma notation expresses summation: \\(
\sum \nolimits_{x : 1}^n f(x)
\\)

The same goes for pi notation for multiplication: \\(
\prod \nolimits_{x : 1}^n f(x)
\\)

When \\(f(x)\\) is a constant \\(c\\) independent of \\(x\\),
\\(\sum \nolimits_{x : 1}^n c = n \times c\\) and
\\(\prod \nolimits_{x : 1}^n c = c^n\\). After all, multiplication is just
repeated addition and exponentation is just repeated multiplication! This reason
is the exact same reason why the depedent sum type is a generalization of the
product type and the dependent product type is a generalization of the sum type.

What happens when \\(B\\) isn't a constant function? Let's look at an example:

\\[
\begin{align}
& f : Bool \to Type \\
& f~True = Unit \\
& f~False = Bool \\
\end{align}
\\]

\\[
\begin{align}
& typ : Type \\
& typ = \sum \nolimits_{b : Bool} f(b) \\
\\
& (True, ()) : typ \\
& (False, True) : typ \\
& (False, False) : typ
\end{align}
\\]

So, \\(typ = f(True) + f(False) = Unit + Bool\\). The types are isomorphic; both
have \\(3\\) inhabitants.

Further reading: [Andrej Bauer's answer to "Why is product type a dependent
SUM?"](https://cs.stackexchange.com/a/81121/84183)

## Types as propositions, terms as proofs

According to what's known as the Curry-Howard correspondence, one can interpret
types and terms as logical propositions and proofs, respectively. By inhabiting
a type, one *proves* the corresponding proposition. This correspondence is named
after Haskell Brooks Curry and William Alvin Howard.

\\[
\begin{align}
& \textbf{Proposition}    && \textbf{Type} \\
& True                    && Unit \\
& False                   && Bottom \\
& A \lor B                && A + B \\
& A \land B               && A \times B \\
& A \to B                 && A \to B \\
& \exists x:A \ldotp B(x) && \sum \nolimits_{x : A} B(x) \\
& \forall x:A \ldotp B(x) && \prod \nolimits_{x : A} B(x)
\end{align}
\\]

The proof of \\(True\\) is trivial: \\(()\\). \\(False\\) has no proof. To prove
\\(A \lor B\\), one must either inhabit \\(A\\) or \\(B\\). To prove
\\(A \land B\\), one must inhabit both \\(A\\) and \\(B\\). To prove
\\(A \to B\\), one must inhabit \\(B\\) given an inhabitant of \\(A\\). To
inhabit \\(\exists x : A \ldotp B(x)\\), one must inhabit both \\(A\\) and
\\(B(x)\\), where \\(B(x)\\) is a proposition about \\(x\\), the inhabitant of
\\(A\\). To inhabit \\(\forall x : A \ldotp B(x)\\), one must show that
\\(B(x)\\) holds, or is inhabited, for every inhabitant \\(x\\) of \\(A\\) that
exists.

Proofs must have a normal form; type systems that accept diverging programs are
inconsistent as a logic. Recursion is a means of introducing nontermination into
a programming language, but induction is important for many proofs. Therefore,
the typechecker needs to make sure that recursive functions are total: They
cover all inputs and never diverge. Alan Turing famously proved that the halting
problem is undecidable; there is no sound and complete algorithm for checking
that a program in a Turing-complete language stops. The implication is that the
termination checker must be conservative; it cannot possibly accept the exact
set of total programs that a Turing-complete language can express.

Proofs expressed with types and terms are also algorithms. They embody the
notion of a computation and describe how to construct a mathematical object. I
think it's exciting that computer science, or the science of computation, has a
place in the foundations of math.

## Types as objects, terms as morphisms

Not only are types propositions and terms morphisms, the simply typed lambda
calculus together with products is the "internal language" of a Cartesian closed
category. The three-way correspondence between type theory, logic, and category
theory is called the Curry-Howard-Lambek correspondence or computational
trinitarianism.

What is a category? A category consists of *objects* and *morphisms*, or arrows
or maps, between these objects. One can compose morphisms, and each object has
an identity morphism, the identity element of composition. This means that
\\(\forall f \ldotp f \circ id = f = id \circ f\\).

The set of morphisms from \\(A\\) to \\(B\\) is written \\(Hom(A, B)\\).

A Cartesian closed category is a category with a "terminal object," denoted
\\(1\\), and for objects \\(A\\) and \\(B\\), a product object \\(A \times B\\)
and an exponential object \\(A^B\\). A terminal object is an object that for
each object \\(A\\), there exists exactly one arrow \\(A \to 1\\). A Cartesian
closed category has these properties:

- There exists a morphism \\(eval : B^A \times A \to B\\). \\(eval\\) is like
  function application!
- \\(Hom(A \times B, C) \cong Hom(A, C^B)\\). Look familiar? This law is
  like the currying of functions!

I'm not knowledgable on category theory, so I may have gotten some things wrong
with the Lambek correspondence.

Further reading: [Computational Trinitarinism on the
nLab](https://ncatlab.org/nlab/show/computational+trinitarianism)

## Practical importance of type theory

Not only is type theory theoretically beautiful, it has practical use as well.
Types, type systems, and typing judgements are the means of specifying a
programming language's static semantics, making statements about a program's
behavior at compile time. More compile-time errors mean less runtime errors.
While tests only check that a program works on certain inputs, compile-time
verification of its properties makes guarantees that it can't go wrong in
certain ways.

Although typechecking will always reject valid programs, types aren't here to
inconvenience you. Done right, a type system helps programmer productivity by
allowing programmers to better specify a program's valid states and expected
behavior. Types serve as formal documentation of APIs.

Mathematicians have used dependent types for real work. For example, the Coq
proof assistant was used to prove the Four-Color Theorem.

## Conclusion

A type truly is a fascinating thing. Is it similar to a set? Is it a
proposition? An object? A description of a computation's behavior? Type theory
has profound and beautiful connections to many fundamental fields of math.

The ideas that I have described are not the full extent of type theory. There
are many other research topics, including but not limited to equality and
homotopy types, codata types, session types, substructural logic, and module
systems. With such a wide wealth of ideas, I am confident that I will never run
out of new things to learn about type theory.
